axios.defaults.baseURL = "http://localhost:8080/admin/franchisee/";

axios.interceptors.request.use(
    function (config) {
        // 在发送请求之前做些什么
        let franchiseeToken = localStorage.getItem('franchiseeToken')
        if (franchiseeToken != null) {
            config.headers.franchiseeToken = franchiseeToken
        }
        return config
    },
    function (error) {
        // 对请求错误做些什么
        return Promise.reject(error)
    }
)

axios.interceptors.response.use(
    function (response) {
        if (response.data.code == 402 || response.data.code == 401) {
            location.href = "login.html"
        }
        return response;
    }, function (error) {
        return Promise.reject(error);
    }
);