axios.defaults.baseURL = "http://localhost:8080/admin/admin/";

axios.interceptors.request.use(
    function (config) {
        // 在发送请求之前做些什么
        let adminToken = localStorage.getItem('adminToken')
        if (adminToken != null) {
            config.headers.adminToken = adminToken
        }
        return config
    },
    function (error) {
        // 对请求错误做些什么
        return Promise.reject(error)
    }
)
axios.interceptors.response.use(
    function (response) {
        if (response.data.code == 402 || response.data.code == 401) {
            location.href = "login.html"
        }
        if (response.data.code == 403) {
            vm.$message.error('无权访问');
        }
        return response;
    }, function (error) {
        return Promise.reject(error);
    }
);